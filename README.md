# SortedArray

[![CI Status](http://img.shields.io/travis/mikhailmulyar/SortedArray.svg?style=flat)](https://travis-ci.org/mikhailmulyar/SortedArray)
[![Version](https://img.shields.io/cocoapods/v/SortedArray.svg?style=flat)](http://cocoapods.org/pods/SortedArray)
[![License](https://img.shields.io/cocoapods/l/SortedArray.svg?style=flat)](http://cocoapods.org/pods/SortedArray)
[![Platform](https://img.shields.io/cocoapods/p/SortedArray.svg?style=flat)](http://cocoapods.org/pods/SortedArray)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

SortedArray is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "SortedArray"
```

## Author

mikhailmulyar, mulyarm@gmail.com

## License

SortedArray is available under the MIT license. See the LICENSE file for more info.
